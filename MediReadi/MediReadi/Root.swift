//
//  Root.swift
//  MediReadi
//
//  Created by Mike on 2016-06-04.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation

class Root{
    static let sharedInstance = Root()
    let PROFILE_KEY = "profile"
    var profile: Profile
    let defaults = NSUserDefaults.standardUserDefaults()

    init(){
        if (defaults.objectForKey(PROFILE_KEY) == nil) {
            defaults.setObject(Profile().toJsonString(), forKey: PROFILE_KEY)
        }
        
        // load profile JSON blob
        let profileBlob = defaults.objectForKey(PROFILE_KEY) as? String
        profile = Profile(json: profileBlob)
    }
    
    func save(){
        defaults.setObject(profile.toJsonString(), forKey: PROFILE_KEY)
    }
}