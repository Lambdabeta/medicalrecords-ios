//
//  TSTableViewController.swift
//  MediReadi
//
//  Created by Ron on 2016-07-16.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import UIKit

extension String {
    struct NumberFormatter {
        static let instance = NSNumberFormatter()
    }
    var floatValue:Float? {
        return NumberFormatter.instance.numberFromString(self)?.floatValue
    }
    var integerValue:Int? {
        return NumberFormatter.instance.numberFromString(self)?.integerValue
    }
}

class EditableCell: UITableViewCell {
    var field: UITextField?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    func setup() {
        self.selectionStyle = .None
        self.field = UITextField(frame: self.frame)
        self.field!.textAlignment = .Right
        self.insertSubview(field!, atIndex: self.subviews.count)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
}

class PointController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var series: TimeSeries
    
    var reuseID = "cell"
    var tableView: UITableView!
    
    let items = ["DateTime","Value"]
    
    let cells: [UITableViewCell]
    
    let dateTimeCell: EditableCell
    let valueCell: EditableCell
    
    var curDate: NSDate?
    var curVal: Float = Float.infinity
    
    var existingPoint: (NSDate, Float)?
    
    func updateDateField(sender:UIDatePicker) {
        curDate = sender.date
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
        dateTimeCell.field!.text = dateFormatter.stringFromDate(sender.date)
        
        self.navigationItem.rightBarButtonItem!.enabled = self.curDate != nil && self.valueCell.field!.text!.floatValue != nil
    }
    
    func updateCurVal(sender: UITextField) {
        curVal = (sender.text! as NSString).floatValue
        self.navigationItem.rightBarButtonItem!.enabled = self.curDate != nil && self.valueCell.field!.text!.floatValue != nil
    }

    init(series: TimeSeries, existingPoint: (NSDate, Float)? = nil) {
        self.existingPoint = existingPoint
        self.series = series
        
        
        self.dateTimeCell = EditableCell()
        self.dateTimeCell.textLabel!.text = "DateTime"
        
        self.valueCell = EditableCell()
        self.valueCell.textLabel!.text = "Value"
        
        if (self.existingPoint != nil) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
            
            self.dateTimeCell.field!.text = dateFormatter.stringFromDate(existingPoint!.0)
            self.valueCell.field!.text = NSString(format: "%.2f", existingPoint!.1) as String
            
            self.curDate = NSDate(timeInterval: 0, sinceDate: existingPoint!.0)
            self.curVal = existingPoint!.1
        }
        
        self.valueCell.field?.keyboardType = .DecimalPad
        self.valueCell.field?.placeholder = "Value in " + self.series.unit
        
        self.cells = [dateTimeCell, valueCell]
        
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dp = UIDatePicker()
        dp.addTarget(self, action: #selector(updateDateField), forControlEvents: UIControlEvents.ValueChanged)
        
        dp.datePickerMode = UIDatePickerMode.DateAndTime
        dateTimeCell.field!.inputView = dp
        
        self.valueCell.field!.addTarget(self, action: #selector(updateCurVal), forControlEvents: UIControlEvents.EditingChanged)
        
        

        tableView = UITableView(frame: self.view.bounds, style: UITableViewStyle.Grouped)
        
        // possibly sketchy. w/o this line, "root" screen width incorrect in landscape
        tableView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: reuseID)
        
        self.view.addSubview(self.tableView)

        self.navigationItem.title = self.existingPoint == nil ? "New Data Point" : "Edit Data Point"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .Plain, target: self, action: #selector(PointController.doneAction))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(PointController.cancelAction))
        
        self.navigationItem.rightBarButtonItem!.enabled = false
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = cells[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        return
    }
    
    func doneAction() {
        if (self.existingPoint != nil) {
            self.series.deletePoint(self.existingPoint!)
        }
        self.series.addPoint((curDate!, curVal))
        Root.sharedInstance.save()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancelAction() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

class TSTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var series: TimeSeries
    var name: String
    
    let reuseID = "cell"
    
    lazy var items: [(NSDate, Float)] = []
    
    var tableView: UITableView!
    
    init(screenName: String, obj: TimeSeries) {
        self.name = screenName
        self.series = obj
        super.init(nibName: nil, bundle: nil)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        items = self.series.getPoints()
        items.sortInPlace { $0.0.compare($1.0) != .OrderedDescending }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView(frame: self.view.bounds, style: UITableViewStyle.Plain)
        
        // possibly sketchy. w/o this line, "root" screen width incorrect in landscape
        tableView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: reuseID)
        
        self.view.addSubview(self.tableView)
        
        self.navigationItem.title = name
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .Plain, target: self, action: #selector(TSTableViewController.addNewPoint))
    }
    
    func addNewPoint() {
        let pointController = PointController(series: self.series)
        pointController.modalTransitionStyle = .CoverVertical
        let nav = UINavigationController(rootViewController: pointController)
        presentViewController(nav, animated: true, completion: nil)
    }
    
    func editPoint(point: (NSDate, Float)) {
        let pointController = PointController(series: self.series, existingPoint: point)
        pointController.modalTransitionStyle = .CoverVertical
        let nav = UINavigationController(rootViewController: pointController)
        presentViewController(nav, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: reuseID)
        let (dateTime, value) = items[indexPath.row]
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/M/yyyy, H:mm"
        let dateString = dateFormatter.stringFromDate(dateTime)
        cell.textLabel?.text = dateString
        cell.detailTextLabel?.text = (NSString(format: "%.2f", value) as String) + " " + self.series.unit
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let this = self
        let index = indexPath.row
        
        // action sheet
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Edit", style: .Default)
        { action -> Void in
            this.editPoint(this.items[index])
            print("Edit")
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        

        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Delete", style: .Destructive)
        { action -> Void in
            this.series.deletePoint(this.items[index])
            print("Delete")
            this.viewWillAppear(false)
            Root.sharedInstance.save()
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
}
