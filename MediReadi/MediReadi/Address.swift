//
//  Name.swift
//  MediReadi
//
//  Created by Mike on 2016-06-04.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import EVReflection

class Address: Node {
    var post_office_box: String = ""
    var extended_address: String = ""
    var street_address: String = ""
    var locality: String = ""
    var region: String = ""
    var postal_code: String = ""
    var country_name: String = ""
}
