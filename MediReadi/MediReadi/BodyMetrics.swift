//
//  BodyMetrics.swift
//  MediReadi
//
//  Created by Ron on 2016-07-16.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation

class BodyMetrics: Node {
    var bodyWeight: TimeSeries = TimeSeries(unit: "lb")
}