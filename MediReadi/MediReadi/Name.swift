//
//  Name.swift
//  MediReadi
//
//  Created by Mike on 2016-06-04.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import EVReflection

class Name: Node {
    var given: String = ""
    var family: String = ""
    var nickname: String = ""
    var formatted: String = ""
    var foo: Foo = Foo()
}

class Foo: Node {
    var bar1: Bar = Bar()
    var bar2: Bar = Bar()
    var fooString: String = ""
    
}

class Bar: Node {
    var given: String = ""
}