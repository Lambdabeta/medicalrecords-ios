//
//  SuperModels.swift
//  MediReadi
//
//  Created by Ron on 2016-07-16.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import EVReflection

// hacky due to EVReflection/Swift compatibility issues
class TimeSeries: EVObject {
    var dateTimes: [String] = []
    var values: [Float] = []
    
    var unit: String = ""
    
    var dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    func getPoints() -> [(NSDate, Float)] {
        var points: [(NSDate, Float)] = []

        if values.count == 0 {
            return points
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        

        for (index, curDateTimeString) in dateTimes.enumerate() {
            let curDateTime = dateFormatter.dateFromString(curDateTimeString)
            let curValue = self.values[index]
            points.append((curDateTime!, curValue))
        }
        
        return points
    }
    
    func deletePoint(toDelete:(NSDate, Float)) -> Bool {
        let (dateTime, _) = toDelete
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        for (index, curDateTimeString) in dateTimes.enumerate() {
            let curDateTime = dateFormatter.dateFromString(curDateTimeString)
            if (dateTime.compare(curDateTime!) == .OrderedSame) {
                dateTimes.removeAtIndex(index)
                values.removeAtIndex(index)
                return true
            }
        }

        return false
    }

    func addPoint(newPoint: (NSDate, Float)) {
        let (dateTime, value) = newPoint
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        let newDateTimeString = dateFormatter.stringFromDate(dateTime)

        for (index, curDateTimeString) in dateTimes.enumerate() {
            let curDateTime = dateFormatter.dateFromString(curDateTimeString)
            if (dateTime.compare(curDateTime!) != .OrderedDescending) {
                dateTimes.insert(newDateTimeString, atIndex: index)
                values.insert(value, atIndex: index)
                return
            }
        }
        dateTimes.append(dateFormatter.stringFromDate(dateTime))
        values.append(value)
        
    }
    
    required init(unit: String) {
        self.unit = unit
        super.init()
    }
    
    required convenience init() {
        self.init()
    }
}

class Node: EVObject {
    
}
