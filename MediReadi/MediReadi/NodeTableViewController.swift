//
//  EVTableViewController.swift
//  MediReadi
//
//  Created by Ron on 2016-07-12.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import UIKit
import FDTextFieldTableViewCell

class NodeTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var obj: Node
    var name: String
    
    let nodeCellID = "nodecell"
    let kvCellID = "kvcell"

    var items: [String] = []
    
    var tableView: UITableView!

    init(screenName: String, obj: Node) {
        self.name = screenName
        self.obj = obj
        super.init(nibName: nil, bundle: nil)
    }

    required convenience init(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        items = obj.toDictionary().allKeys as! [String]
        items.sortInPlace()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView(frame: self.view.bounds, style: UITableViewStyle.Plain)

        // possibly sketchy. w/o this line, "root" screen width incorrect in landscape
        tableView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]

        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: nodeCellID)
//        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: nodeCellID)
        
        self.view.addSubview(self.tableView)
        
        self.navigationItem.title = name
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let key = items[indexPath.row]
        let val = obj.valueForKey(key)
        
        if val is Node || val is TimeSeries {
            var cell = tableView.dequeueReusableCellWithIdentifier(nodeCellID)
            if (cell == nil) {
                cell = UITableViewCell(style: .Default, reuseIdentifier: nodeCellID)
            }
            cell!.textLabel?.text = items[indexPath.row]
            cell!.accessoryType = .DisclosureIndicator
            return cell!
        } else if val is String {
            var cell = tableView.dequeueReusableCellWithIdentifier(kvCellID)
            if (cell == nil) {
                cell = UITableViewCell(style: .Value1, reuseIdentifier: kvCellID)
            }

            let key = items[indexPath.row]
            cell!.textLabel?.text = key
            cell!.detailTextLabel?.text = obj.valueForKey(key) as? String
            return cell!
        }
        
        // lolwut
        return UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: nodeCellID)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let key = items[indexPath.row]
        let val = obj.valueForKey(key)
        
        if val is Node {
            let vc = NodeTableViewController(screenName: key, obj: (val as? Node)!)
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.navigationController?.pushViewController(vc, animated: true)
        } else if val is TimeSeries {
            let vc = TSTableViewController(screenName: key, obj: (val as? TimeSeries)!)
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.navigationController?.pushViewController(vc, animated: true)
        } else if val is String {
            let key = items[indexPath.row]
            
            var field: UITextField?
            
            let alertController = UIAlertController(title: "Editing", message: key, preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                self.obj.setValue(field?.text, forKey: key)
                Root.sharedInstance.save()
                tableView.reloadData()
            })
            let cancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
                return
            }
            alertController.addAction(ok)
            alertController.addAction(cancel)
            alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                field = textField
                field?.text = self.obj.valueForKey(key) as? String
            }
            
            presentViewController(alertController, animated: true, completion: nil)
            
            tableView.deselectRowAtIndexPath(indexPath, animated: false)
        }
        
    }
}
