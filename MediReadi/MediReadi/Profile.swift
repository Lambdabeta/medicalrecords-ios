//
//  Profile.swift
//  MediReadi
//
//  Created by Ron on 2016-06-01.
//  Copyright © 2016 fydp. All rights reserved.
//

import Foundation
import EVReflection

class Profile: Node {
    var name: Name = Name()
    var address: Address = Address()
    var bodyMetrics: BodyMetrics = BodyMetrics()
    
    func saveAll(sender: UIButton){
        Root.sharedInstance.save()
    }
}